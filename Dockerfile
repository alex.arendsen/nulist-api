# Based guide from Microsoft's docs on Docker Hub
# https://hub.docker.com/r/microsoft/aspnetcore-build/

# Build project
FROM microsoft/aspnetcore-build:2.0-jessie
LABEL Name=nulist-api
LABEL Version=0.1.0
WORKDIR /source

COPY nulist-api.csproj .
RUN dotnet restore

COPY . .
RUN dotnet publish --output /app/ --configuration Release

# Run built project
# The docs also say you should use the microsoft/aspnetcore runtime container
# to run the project in an env that doesn't have the SDK, but when you do that
# all it does is complain that `dotnet` doesn't exist... so we're just doing it
# this way.
WORKDIR /app

# _Not_ `dotnet nulist-api.dll` like what the docs say to do...
ENTRYPOINT ["dotnet", "run"]
